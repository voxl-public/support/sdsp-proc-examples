# sDSP/SLPI Processor Examples

This project contains source code examples targeting the VOXL Applications DSP processor (sDSP/SLPI). sDSP programs comprise of two components: sDSP libraries and CPU processor executable. CPU binaries produce corresponding remote method invocations (RMI) from the ADSP libraries.

Following this scheme, all sample projects comprise of an adsp and an apps component that must be build separately in their respective environments.

* Initialize repository submodules (including all sample dependencies) recursively before building sample projects.

```
> git submodule update --init --recursive
```
