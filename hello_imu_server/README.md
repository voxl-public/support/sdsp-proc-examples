# SLPI IMU Server API Implementation
This project **requires the Hexagon SDK Cross-Compiler Environment** available at voxl-docker (https://gitlab.com/voxl-public/voxl-docker).

0. Copy libsensor_imuserver.so from VOXL (/usr/lib/rsfa/adsp) using ADB. This .so is part of imu_app package. Plase the .so into sdsp/ directory.

1. Build sDSP/SLPI libraries and APP processor binary
```bash
$ cd hello_imu_server
$ voxl-docker -i voxl-hexagon
$ ./build.sh
```

2. Confirm that target is found with adb
```bash
> adb devices
List of devices attached
1a2b3c4d	device
```

3. Push binaries and libraries to target
```bash
$ ./deploy_to_target
```

4. To view output from the DSP run Hexagon debug tool mini-dm (within voxl-hexagon docker image)
```bash
$ ${HEXAGON_SDK_ROOT}/tools/debug/mini-dm/Linux_Debug/mini-dm
```

5. In a new shell run example on target
```bash
$ adb shell
$ cd home/root
$ ./hello_imu_server
```

You should see the following output in the terminal running hello_imu_server:
```
~ # ./hello_imu_server
Asking DSP to start IMU server
Success!
Waiting for test data to be geneerated...
Done
Asking DSP to stop IMU server
Success!
```

You should see the following output in the terminal running mini-dm:
```
Running mini-dm version: 3.1
<various initialization outputs>
DMSS is connected. Running mini-dm...
----------------Mini-dm is ready to log----------------
<warnings you can ignore>
[08500/00]  --:--.---  Starting IMU  0160  hello_imu_server.c
[08500/00]  --:--.---  New thread creation was successful  0186  hello_imu_server.c
[08500/00]  --:--.---  Hello from inside the new thread  0067  hello_imu_server.c
[08500/02]  --:--.---  [adsp] getting driver configuration   0177  sensor_imu_impl.cpp
[08500/02]  --:--.---  Deleting IMU Manager instance.  No pending clients  0092  SensorImuManagerFactory.cpp
[08500/00]  --:--.---  Exiting the new thread  0131  hello_imu_server.c
[08500/00]  --:--.---  New thread finished  0152  hello_imu_server.c
```

While the IMU server is running, you can test it using ```sensor_imu_tester``` app (also from imu_app package).
```
sensor_imu_tester -d 3
```
will connect to the IMU server on DSP and grab data for 3 seconds and write it to a file in the same directory. If there is an error with IMU server, the test will fail.
