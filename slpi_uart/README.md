# SLPI UART MAVLink Message Forwarding Example
This is a proxy application that handles the MAVLink message exchange between the PX4 serial port and UDP connection.

## Build Source
This project **requires the Hexagon SDK Cross-Compiler Environment** available at voxl-docker (https://gitlab.com/voxl-public/voxl-docker).

1. Build sDSP/SLPI libraries and APP processor binary
```bash
$ cd slpi_uart
$ voxl-docker -i voxl-hexagon
$ ./build.sh
```

2. Confirm that target is found with adb
```bash
> adb devices
List of devices attached 
1a2b3c4d	device
```

3. Push binaries and libraries to target
```bash
$ ./deploy_to_target
```

## Installation
If preferred, a pre-built package is available at https://developer.modalai.com.

To generate the IPK from source, perform the following:

```bash
cd slpi_uart
./make_ipk.sh
```

To install, connect the VOXL via USB and run the following:

```bash
./install_on_target.sh
```
